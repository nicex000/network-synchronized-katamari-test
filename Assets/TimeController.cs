using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

public class TimeController : MonoBehaviour
{
    [SerializeField]
    private float m_TimeScale = 1f;

    private bool m_bUseOriginalTimeScale = true;
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            if (m_bUseOriginalTimeScale)
            {
                m_bUseOriginalTimeScale = false;
                Time.timeScale = m_TimeScale;
            }
            else
            {
                m_bUseOriginalTimeScale = true;
                Time.timeScale = 1f;
            }
        }
    }
}

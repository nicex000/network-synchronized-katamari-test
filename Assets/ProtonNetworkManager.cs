using System;
using System.Collections;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class ProtonNetworkManager : MonoBehaviourPunCallbacks, IPunObservable
{
    // Start is called before the first frame update

    [SerializeField] private int SyncIterations;
    [SerializeField] private PhotonView PhotonViewComponent;

    [SerializeField] private Transform PlayerCube;
    [SerializeField] private Transform SmallCubeContainer;

    private int syncIter = 100;
    private List<CubeData> dataToSend = new List<CubeData>(500);
    void Start()
    {
        PhotonPeer.RegisterType(typeof(CubeData), 0, CubeData.Serialize, CubeData.Deserialize);
        PhotonNetwork.ConnectUsingSettings();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting) OnWrite(ref stream, ref info);
        else if (stream.IsReading) OnRead(ref stream, ref info);
    }

    private void OnWrite(ref PhotonStream stream, ref PhotonMessageInfo info)
    {
        stream.SendNext(PlayerCube.position);
        stream.SendNext(PlayerCube.rotation);

        Int16 childIndex = 0;
        foreach (Transform smallCube in SmallCubeContainer.GetComponentsInChildren<Transform>())
        {
            if (smallCube == SmallCubeContainer) continue;
            if (smallCube.GetComponent<Rigidbody>().velocity != Vector3.zero || syncIter == 0)
            {
                CubeData cubeData = new CubeData();
                cubeData.Write(smallCube.position, smallCube.rotation, 
                    smallCube.GetComponent<Rigidbody>().velocity != Vector3.zero, childIndex);
                dataToSend.Add(cubeData);
            }
            ++childIndex;
        }

        stream.SendNext(dataToSend.Count);
        foreach (CubeData cubeToSend in dataToSend)
        {
            stream.SendNext(cubeToSend);
        }
        dataToSend.Clear();
        ++syncIter;
        if (syncIter >= SyncIterations)
        {
            syncIter = 0;
            Debug.Log("Syncing all cubes");
        }
    }

    private void OnRead(ref PhotonStream stream, ref PhotonMessageInfo info)
    {
        PlayerCube.GetComponent<NetworkedTransformComponent>().UpdateRemotePositionAndRotation(
            (Vector3)stream.ReceiveNext(), (Quaternion)stream.ReceiveNext());


        int receivedCubeStates = (int)stream.ReceiveNext();
        for (int i = 0; i < receivedCubeStates; ++i)
        {
            CubeData smallCubeData = (CubeData)stream.ReceiveNext();
            Transform smallCube = SmallCubeContainer.transform.GetChild(smallCubeData.index);

            Vector3 pos = Vector3.zero;
            Quaternion rot = Quaternion.identity;
            smallCubeData.Read(ref pos, ref rot);

            smallCube.GetComponent<NetworkedTransformComponent>().UpdateRemotePositionAndRotation(pos, rot);
            if (smallCubeData.isInteracting) smallCube.GetComponent<SmallCubeComponent>().MakeRed();
        }
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();

        RoomOptions op = new RoomOptions();
        op.MaxPlayers = 2;
        op.IsVisible = true;
        
        Debug.Log("Joining room...");
        PhotonNetwork.JoinOrCreateRoom("Networked Katamari Test", op, TypedLobby.Default);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();

        Debug.Log("Room Joined.");

        if (!photonView.IsMine)
        {
            PlayerCube.GetComponent<Rigidbody>().isKinematic = true;
            PlayerCube.GetComponent<CubeController>().enabled = false;
            PlayerCube.GetComponent<NetworkedTransformComponent>().enabled = true;
            foreach (Rigidbody smallCube in SmallCubeContainer.GetComponentsInChildren<Rigidbody>())
            {
                smallCube.isKinematic = true;
                smallCube.GetComponent<NetworkedTransformComponent>().enabled = true;
            }
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class ChaseScript : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private Transform ChaseObject;
    [SerializeField] private float SmoothingTime;

    private Vector3 offsetFromChaseObject;
    private Vector3 outVel;
    void Start()
    {
        offsetFromChaseObject = this.transform.position - ChaseObject.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        this.transform.position = Vector3.SmoothDamp(transform.position, 
            ChaseObject.position + offsetFromChaseObject, ref outVel, SmoothingTime);
    }
}

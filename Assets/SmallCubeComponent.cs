using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallCubeComponent : MonoBehaviour
{

    [SerializeField] private float ActiveTime;
    [SerializeField] private float ActiveFalloffTime;
    [SerializeField] private Color redColor;
    private float timer;

    private bool isActive;

    private Material mat;

    private Color defaultColor;

    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0f;
        isActive = false;
        mat = GetComponent<MeshRenderer>().material;
        defaultColor = mat.color;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rb.velocity.sqrMagnitude > 0) MakeRed();

        if (isActive)
        {
            timer -= Time.deltaTime;
            if (timer <= 0f)
            {
                timer = ActiveFalloffTime;
                isActive = false;
            }
        }
        else if (timer > 0)
        {
            timer -= Time.deltaTime;

            if (timer < 0) timer = 0;

            mat.color = Color.Lerp(defaultColor, redColor, timer / ActiveFalloffTime);
        }
    }

    public void MakeRed()
    {
        isActive = true;
        timer = ActiveTime;
        mat.color = redColor;
    }
}

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class CubeController : MonoBehaviour
{
    [SerializeField]
    private float m_MaxSpeed = 10f;
    [SerializeField]
    private float m_MaxAngSpeed = 10f;
    [SerializeField]
    private float m_JumpSpeed = 10f;
    [SerializeField]
    private float m_FlyHeight = 10f;

    [SerializeField]
    private float m_KnockbackForce = 10f;
    [SerializeField]
    private float m_attractForce = 10f;
    [SerializeField]
    private float m_repelForce = 10f;

    [SerializeField] private Collider m_KnockbackSphere;
    [SerializeField] private Collider m_AttractSphere;
    [SerializeField] private GameObject m_RepelCapsule;

    private Rigidbody m_Rb = null;

    private Vector3 intendedDirection;
    private Vector3 intendedTorque;

    private bool jumpPressed, wantingToFly, isFlying, isAttracting;

    private void Awake()
    {
        m_Rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        jumpPressed = Input.GetButtonDown("Jump") && transform.position.y < 2f;
        if (jumpPressed) Jump();
        wantingToFly = Input.GetButton("Jump");
        isAttracting = Input.GetButton("Fire1");
        if (Input.GetButtonDown("Fire2")) Knockback();



        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        intendedDirection = new Vector3(horizontal, 0f, vertical).normalized;
        intendedTorque = new Vector3(vertical, 0f, -horizontal).normalized;
    }

    private void FixedUpdate()
    {
        if (m_Rb == null)
        {
            return;
        }

        if (wantingToFly)
        {
            if (!isFlying)
            {
                if (transform.position.y >= m_FlyHeight)
                {
                    isFlying = true;
                }
            }
            else
            {
                if (transform.position.y < m_FlyHeight)
                {
                    m_Rb.constraints = RigidbodyConstraints.FreezePositionY;
                }
            }
        }
        else if (m_Rb.constraints == RigidbodyConstraints.FreezePositionY)
        {
            m_Rb.constraints = RigidbodyConstraints.None;
        }
        if (transform.position.y < 2f || !wantingToFly)
        {
            isFlying = false;
        }

        if (isAttracting)
        {
            if (isAttracting) Attract();
            m_RepelCapsule.SetActive(false);
        }
        else if (isFlying) Repel();
        else
        {
            m_RepelCapsule.SetActive(false);
        }
        
        //m_Rb.velocity = velocity;
        m_Rb.AddForce(intendedDirection * m_MaxSpeed * Time.fixedDeltaTime);
        m_Rb.AddTorque(intendedTorque * m_MaxAngSpeed * Time.fixedDeltaTime);
    }

    private void Jump()
    {
        m_Rb.AddForce(Vector3.up * m_JumpSpeed, ForceMode.Impulse);
    }

    private void GetSmallCubesInOverlapSphere(ref List<Rigidbody> OutRigidbodies, Collider overlapSphere)
    {
        OutRigidbodies.Clear();
        
        Collider[] colliders = Physics.OverlapSphere(overlapSphere.bounds.center, overlapSphere.bounds.extents.x);
        foreach (var coll in colliders)
        {
            SmallCubeComponent comp = coll.gameObject.GetComponent<SmallCubeComponent>();
            if (comp != null)
            {
                OutRigidbodies.Add(comp.GetComponent<Rigidbody>());
            }
        }
    }

    private void Knockback()
    {
        List<Rigidbody> objsToKnockback = new List<Rigidbody>();
        GetSmallCubesInOverlapSphere(ref objsToKnockback, m_KnockbackSphere);

        foreach (var item in objsToKnockback)
        {
            item.AddForce((item.transform.position - m_KnockbackSphere.bounds.center).normalized * m_KnockbackForce,
                ForceMode.Impulse);
        }
    }

    private void Attract()
    {
        List<Rigidbody> objsToAttract = new List<Rigidbody>();
        GetSmallCubesInOverlapSphere(ref objsToAttract, m_AttractSphere);

        foreach (var item in objsToAttract)
        {
            item.AddForce((item.transform.position - m_AttractSphere.bounds.center).normalized * 
                          -m_attractForce * Time.fixedDeltaTime);
        }
    }

    private void Repel()
    {
        m_RepelCapsule.SetActive(true);
        m_RepelCapsule.transform.position = this.transform.position;
        m_RepelCapsule.transform.rotation = Quaternion.identity;
        /*List<Rigidbody> objsToRepel = new List<Rigidbody>();
        GetSmallCubesInOverlapSphere(ref objsToRepel, m_AttractSphere);

        foreach (var item in objsToRepel)
        {
            item.AddForce((item.transform.position - m_AttractSphere.bounds.center).normalized *
                          m_repelForce * Time.fixedDeltaTime);
        }
        */


    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PosType = System.Int16;
using RotType = System.Byte;

public class CubeData
{
    private static int posMinLimit = -1024;
    private static int posMaxLimit = 1024;
    private PosType pX;
    private PosType pY;
    private PosType pZ;
    private RotType rX;
    private RotType rY;
    private RotType rZ;

    public bool isInteracting;
    public Int16 index;

    public static UInt16 GetSizeInBytes()
    {
        return sizeof(PosType) * 3 + sizeof(RotType) * 3 + sizeof(bool) + sizeof(UInt16);
    }

    public void Write(Vector3 position, Quaternion rotation, bool interacting, Int16 cubeIndex)
    {
        pX = (PosType)ClampAndRemapRange(position.x, 
            posMinLimit, posMaxLimit, PosType.MinValue, PosType.MaxValue);
        pY = (PosType)ClampAndRemapRange(position.y, 
            posMinLimit, posMaxLimit, PosType.MinValue, PosType.MaxValue);
        pZ = (PosType)ClampAndRemapRange(position.z, 
            posMinLimit, posMaxLimit, PosType.MinValue, PosType.MaxValue);

        int wPos = (rotation.w >= 0) ? 1 : -1;
        rX = (RotType)ClampAndRemapRange(wPos * rotation.x, 
            -1, 1, RotType.MinValue, RotType.MaxValue);
        rY = (RotType)ClampAndRemapRange(wPos * rotation.y, 
            -1, 1, RotType.MinValue, RotType.MaxValue);
        rZ = (RotType)ClampAndRemapRange(wPos * rotation.z, 
            -1, 1, RotType.MinValue, RotType.MaxValue);

        isInteracting = interacting;
        index = cubeIndex;
    }

    public void Read(ref Vector3 position, ref Quaternion rotation)
    {
        position.x = ClampAndRemapRange(pX, 
            PosType.MinValue, PosType.MaxValue, posMinLimit, posMaxLimit);
        position.y = ClampAndRemapRange(pY, 
            PosType.MinValue, PosType.MaxValue, posMinLimit, posMaxLimit);
        position.z = ClampAndRemapRange(pZ, 
            PosType.MinValue, PosType.MaxValue, posMinLimit, posMaxLimit);

        rotation.x = ClampAndRemapRange(rX, RotType.MinValue, RotType.MaxValue, -1, 1);
        rotation.y = ClampAndRemapRange(rY, RotType.MinValue, RotType.MaxValue, -1, 1);
        rotation.z = ClampAndRemapRange(rZ, RotType.MinValue, RotType.MaxValue, -1, 1);
        rotation.w = Mathf.Sqrt(1 - (rotation.x * rotation.x + rotation.y * rotation.y + rotation.z * rotation.z));
    }

    public static byte[] Serialize(object customType)
    {
        var cubeData = (CubeData)customType;
        byte[] serializedByteArray = new byte[GetSizeInBytes()];

        Array.Copy(BitConverter.GetBytes(cubeData.pX), 
            0, serializedByteArray, 0, 2);
        Array.Copy(BitConverter.GetBytes(cubeData.pY), 
            0, serializedByteArray, 2, 2);
        Array.Copy(BitConverter.GetBytes(cubeData.pZ), 
            0, serializedByteArray, 4, 2);
        serializedByteArray[6] = cubeData.rX;
        serializedByteArray[7] = cubeData.rY;
        serializedByteArray[8] = cubeData.rZ;
        serializedByteArray[9] = Convert.ToByte(cubeData.isInteracting);
        Array.Copy(BitConverter.GetBytes(cubeData.index), 
            0, serializedByteArray, 10, 2);

        return serializedByteArray;
    }

    public static object Deserialize(byte[] data)
    {
        var result = new CubeData();
        result.pX = BitConverter.ToInt16(data, 0);
        result.pY = BitConverter.ToInt16(data, 2);
        result.pZ = BitConverter.ToInt16(data, 4);
        result.rX = data[6];
        result.rY = data[7];
        result.rZ = data[8];
        result.isInteracting = Convert.ToBoolean(data[9]);
        result.index = BitConverter.ToInt16(data, 10);

        return result;
    }

    static float ClampAndRemapRange(float value, float iMin, float iMax, float oMin, float oMax)
    {
        float t = Mathf.InverseLerp(iMin, iMax, value);
        return Mathf.Lerp(oMin, oMax, t);
    }
}

using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UIElements;

public class NetworkedTransformComponent : MonoBehaviour
{

    [SerializeField] private bool UseInterpolation;
    [SerializeField] private float InterpolationSpeed = 50f;
    [SerializeField] private float Precision = 1f;


    private Vector3 RemotePosition;
    private Quaternion RemoteRotation;
    public void UpdateRemotePositionAndRotation(Vector3 newPos, Quaternion newRot)
    {
        RemotePosition = newPos;
        RemoteRotation = newRot;
    }

    // Start is called before the first frame update
    private void Start()
    {
        RemotePosition = transform.position;
        RemoteRotation = transform.rotation;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!UseInterpolation)
        {
            transform.position = RemotePosition;
            transform.rotation = RemoteRotation;
        }
        else
        {
            if (!transform.position.AlmostEquals(RemotePosition, Precision))
                transform.position =
                    Vector3.Lerp(transform.position, RemotePosition, Time.deltaTime * InterpolationSpeed);

            if (!transform.rotation.AlmostEquals(RemoteRotation, Precision))
                transform.rotation = 
                    Quaternion.Slerp(transform.rotation, RemoteRotation, Time.deltaTime * InterpolationSpeed);
        }
    }
}

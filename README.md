
# Network Synchronized Katamari Test

A simple networking test using Photon



## Setup

As you will need 2 instances (the "server" to play on, and a "host" to replicate the world),
there are a few ways you can set it up:

#### Using a build
Build the game, and run the game along with the editor.

#### 2 editors
Copy the entire project into a seperate folder and run 2 editors.\
or\
Copy the project without the assets folder and create a junction to share the assets between 2 instances of the project.

## Run

Start the first instance, this will be the one you play on (the server).\
Start the second instance, this will be one that replicates the world (the host).

## How to play

#### Move
WASD to move

#### Jump and Fly
Spacebar to jump, hold to fly\
Flying pushes the small cubes away

#### Attract
Hold LMB to attract the small cubes

#### Knockback
Hold RMB to knockback the small cubes
## Unity
#### Version
 - 2020.3.32f1
#### Dependencies
 - [Photon Unity Networking v2.40 (PUN)](https://www.photonengine.com)

